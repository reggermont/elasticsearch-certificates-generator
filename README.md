# ElasticSearch Certificates Generator

for elastic search 6.x (not guaranteed to work for 7.x)

## How to generate certificates

- Run `make` command
- Down your elasticsearch container and remove the image with `docker rmi ${ES_IMAGE_NAME}` 
- Copy the content of `certs` folder into your project
