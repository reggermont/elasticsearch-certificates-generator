# Changelog

[Return to README](README.md)

## Summary
* [UNRELEASED](#UNRELEASED)
* [1.0.0 (2019-12-26)](#100-2019-12-26)

[Return to summary](#summary)
## UNRELEASED

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

[Return to summary](#summary)
## 1.0.0 (2018-12-13)

### Added
* Initial release

[Return to summary](#summary)
