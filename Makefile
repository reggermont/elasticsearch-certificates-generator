CERTS_FOLDER = certs
ES_CERTS_FOLDER = $(CERTS_FOLDER)/elasticsearch

certs-create:
	docker-compose up
	rm $(CERTS_FOLDER)/bundle.zip
	cat $(ES_CERTS_FOLDER)/elasticsearch.key > $(ES_CERTS_FOLDER)/elasticsearch.pem
	cat $(ES_CERTS_FOLDER)/elasticsearch.crt >> $(ES_CERTS_FOLDER)/elasticsearch.pem
